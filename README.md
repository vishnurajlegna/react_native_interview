# LegnaStudios React Native Developer Interview Guide

Welcome to LegnaStudios React-native Interview Project Guide. 

In this guide, we'll walk through the project you're going to build with our API.


# Requirements.

- Create a scroll view as shown in the screenshot below [sample_UI in repo] and populate the views using the api get_latest 
- Create minimum 2 post showing the data. 
- Run the app on both ios and android and send us the code and screenshot. [Gif if possible]

[Screenshot 1](https://bitbucket.org/vishnurajlegna/react_native_interview/src/master/sample_ui.jpeg)



# get_latest  JSON Data:

-   HTTP `GET` request to:
    
    `https://vishnurajn.com/test_api/api/interview/get_latest.php?user_id=1&offset=1`
    
-   Sample response of one `get_latest` object

 `[ "latestPosts": [{"post_id": "235634263650417","post_created_time": "2018-04-01T15:01:02+0000","post_user_id": "1","post_user_name": "charan vadlapati","post_user_image_url": "","post_pen_name": "charan vadlapati","post_text_language": "","post_description": "MicroStories by Charan Vadlapati\n\nGet it on google play: https:\/\/play.google.com\/store\/apps\/details?id=com.appsoflife.microstories\n#microStories #flashFiction #microfiction #nanotales  #Pity #Quotes #Poems #spilledink","post_description_language": "","post_likes": "0","post_emotions": "","post_image_720": "https:\/\/scontent-lax3-2.xx.fbcdn.net\/v\/t1.0-9\/29695106_235634263650417_2005012927861008532_n.png?_nc_cat=107&ccb=1-3&_nc_sid=caaa8d&_nc_ohc=y5raR2GYi3wAX88Tw3i&_nc_ht=scontent-lax3-2.xx&oh=a2eca82ae6d3af160d8b1305a3aae1a2&oe=607FE9B4","likesIndicator": false,"bookmarkIndicator": false} ]`.


# Submission
Push your project to your GitHub or any other Git version control platform and share the repo URL with us. Or email us as a zip file info@legnastudios.com

![ScreenShot](https://bitbucket.org/vishnurajlegna/react_native_interview/src/master/sample_ui.jpeg)


